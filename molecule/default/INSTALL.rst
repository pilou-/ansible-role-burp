******
How to
******

Requirements
============

* Docker Engine

Install
=======

Please refer to the `Virtual environment`_ documentation for installation best
practices.

.. _Virtual environment: https://virtualenv.pypa.io/en/latest/

.. code-block:: bash

    $ pip install -r molecule/requirements.txt

Usage
=====

    $ molecule test
