"""Role testing files using testinfra."""
import os
import os.path
import re
from time import sleep

import pytest
import testinfra


testinfra_hosts = ['molecule-burp-client']

client_conf = '/etc/burp/test-backup/molecule-burp-server-molecule-burp-client-documents.conf'
burp_command = '/usr/sbin/burp -a {action} -c {conf}'
canary_updated = 'coincoin'


@pytest.fixture(scope="function")
def restore_dir(host):
    # temp directory will belongs to root
    restore_dir = host.ansible('tempfile', 'state=directory', check=False)
    yield restore_dir['path']
    host.ansible('file', f"path={restore_dir['path']} state=absent", check=False)


def test_check_backup(host):
    '''Check exactly one backup exists'''

    # wait for the first backup which is triggered by a cron job executed every minute
    inventory = os.environ['MOLECULE_INVENTORY_FILE']
    uri = 'ansible://%s?ansible_inventory=%s'
    burp_server = testinfra.get_host(uri % ('molecule-burp-server', inventory))
    wait_cmd = 'zgrep -q "Backup completed" ' \
               '/var/local/backups-burp/test-backup/molecule-burp-client-documents/current/log.gz'
    for i in range(0, 14):  # 70 seconds max
        sleep(5)
        check = burp_server.ansible('command', wait_cmd, check=False)
        if check['rc'] == 0:
            break
    else:
        assert False, "Backup wasn't completed on server"

    list_backups_cmd = burp_command.format(action='l', conf=client_conf)
    backups_list = host.ansible('command', list_backups_cmd, check=False)
    assert backups_list['rc'] == 0
    backups = re.findall("^Backup: [0-9]{7}", backups_list['stdout'], re.MULTILINE)
    assert len(backups) == 1
    assert backups[0] == 'Backup: 0000001'


def test_list_content(host):
    '''Check that canary filename is listed in backup'''
    host_vars = host.ansible.get_variables()
    canary_path = host_vars['canary_path']
    canary_filename = os.path.basename(canary_path)

    list_content_cmd = burp_command.format(action='l', conf=client_conf)
    list_content_cmd += ' -r %s' % canary_filename
    list_content = host.ansible('command', list_content_cmd, check=False)
    assert list_content['rc'] == 0

    canary_path_rendered = host.ansible('debug', 'msg=%s' % canary_path, check=False)
    assert canary_path_rendered['msg'] in list_content['stdout_lines']


def restore_and_check(host, restore_dir, canary, canary_path):
    '''Restore and check content'''

    restore_cmd = burp_command.format(action='r', conf=client_conf)
    restore_cmd += ' -d %s -r %s -s %d' % (restore_dir, canary_path, canary_path.count(os.path.sep))
    restore = host.ansible('command', restore_cmd, check=False)
    assert restore['rc'] == 0
    assert restore['stdout'].endswith('restore finished')

    restored_filename = os.path.basename(canary_path)
    restored_path = os.path.join(restore_dir, restored_filename)
    assert canary == host.file(restored_path).content_string


def test_restore(host, restore_dir):
    '''Restore and check content'''
    host_vars = host.ansible.get_variables()
    canary = host_vars['canary']
    canary_path = host_vars['canary_path']

    restore_and_check(host, restore_dir, canary, canary_path)


def test_backup(host):
    '''force a new backup'''
    host_vars = host.ansible.get_variables()
    canary_path = host_vars['canary_path']
    test_user = host_vars['test_user']

    host.ansible("copy", f"content={canary_updated} dest={canary_path} owner={test_user} group={test_user}",
                 check=False)
    backup_cmd = burp_command.format(action='b', conf=client_conf)
    backup = host.ansible('command', backup_cmd, check=False)
    assert backup['rc'] == 0
    assert backup['stdout'].endswith('backup finished ok')


def test_list_new_backup(host):
    '''Check exactly two backups exist'''
    list_backups_cmd = burp_command.format(action='l', conf=client_conf)
    backups_list = host.ansible('command', list_backups_cmd, check=False)
    assert backups_list['rc'] == 0
    backups = re.findall("^Backup: [0-9]{7}", backups_list['stdout'], re.MULTILINE)
    assert len(backups) == 2
    assert backups[0] == 'Backup: 0000001'
    assert backups[1] == 'Backup: 0000002'


def test_new_backup_content(host, restore_dir):
    '''Restore and check content'''
    host_vars = host.ansible.get_variables()
    canary_path = host_vars['canary_path']

    restore_and_check(host, restore_dir, canary_updated, canary_path)
