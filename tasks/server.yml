---
- name: include distribution specific variables
  include_vars: '{{ item }}'
  with_first_found:
    - files:
        - '{{ ansible_distribution }}-{{ ansible_distribution_major_version }}.yml'
        - '{{ ansible_os_family }}.yml'
      paths: ../vars

- name: Create main BURP directory backup
  file:
    path: '{{ burp.dir|default(burp_rootdir, true) }}'
    state: directory
    owner: root
    group: root
    mode: 0751

- name: Create dedicated group
  group:
    name: 'burp-{{ burp.name }}'
    system: yes
    state: present

- name: Create dedicated user
  user:
    name: 'burp-{{ burp.name }}'
    group: 'burp-{{ burp.name }}'
    system: yes
    createhome: no
    state: present

- vars:
    stub1: &owner
      owner: 'burp-{{ burp.name }}'
      group: 'burp-{{ burp.name }}'
    stub2: &owner-file
      mode: 0400
      <<: *owner
  block:
    - name: Create BURP conf directory
      file:
        path: '/etc/burp/{{ burp.name }}/'
        state: directory
        mode: '0750'
        <<: *owner

    - name: COPY CA server
      copy:
        content: '{{ burp.ssl.CA }}'
        dest: '/etc/burp/{{ burp.name }}/ca.crt'
        mode: '0440'
        <<: *owner

    - name: Create BURP default spool directory
      file:
        path: '{{ burp.dir|default(burp_rootdir, true) }}/{{ burp.name }}/default'
        state: directory
        mode: 0700
        <<: *owner

    - name: Create BURP client conf directory
      file:
        path: '/etc/burp/{{ burp.name }}/clientconfdir'
        state: directory
        mode: 0700
        <<: *owner

    - name: COPY certificate server
      copy:
        content: '{{ burp.ssl.cert }}'
        dest: '/etc/burp/{{ burp.name }}/burp.crt'
        <<: *owner-file
      notify: 'reload burp service'

    - name: COPY key certificate server
      copy:
        content: '{{ burp.ssl.key }}'
        dest: '/etc/burp/{{ burp.name }}/burp.key'
        <<: *owner-file
      diff: no
      notify: 'reload burp service'

    - name: COPY DH param
      copy:
        content: '{{ burp.ssl.DH }}'
        dest: '/etc/burp/{{ burp.name }}/dh2048.pem'
        <<: *owner-file
      notify: 'reload burp service'

    - name: Fetch burp version
      import_tasks: fetch_burp_version.yml

    - name: 'Generate BURP server configuration'
      vars:
        burp_server_version: '{{ burp_version.stdout.strip().split("-")[-1] }}'
      template:
        src: server.conf
        dest: '/etc/burp/{{ burp.name }}/server.conf'
        <<: *owner-file
      notify: 'reload burp service'

    - name: 'Workaround for #532 (fix not available in Debian Stretch)'
      include: 'server_workaround_#532.yml'
      when: ansible_os_family == 'Debian'

    - include_tasks: '{{ ansible_service_mgr }}/enable_burp.yml'
