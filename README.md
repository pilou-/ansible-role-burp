# Ansible role for Burp installation

This role allows to setup [Burp](http://burp.grke.org/) servers and clients:
hence the role parameter named `type` must be set to `server` or `client`:

```yaml
roles:
  - { role: burp, type: 'server' }
```
or
```yaml
roles:
  - { role: burp, type: 'client' }
```

Each Burp client is allowed to define multiple configurations with different
parameters, including:
  * paths to include
  * paths to exclude
  * retention time.

This role handles multiple Burp servers running on the same host.

## Requirements/Compatibility (server and client hosts)

  * OS: Debian Stretch is supported
  * init system: systemd is required
  * required command: `openssl` (client hosts)

If installed and running, `rsyslog` will be used (and then a `logrotate`
configuration file will be created too).

## Variables

### Server default variables:

  * `burp_rootdir`: `/var/local/backups-burp`
  * `burp_client_can_delete`: `0`
  * `burp_client_can_force_backup`: `0`
  * `burp_client_can_list`: `1`
  * `burp_client_can_restore`: `1`
  * `burp_client_can_verify`: `1`
  * `burp_exclude_comp_exts`: extensions to exclude from compression case
                          insensitive: bz,bz2,zip,gz,webm,mov,png,jpg,jpeg,rar,
                          m4v,avi,xv,jar,tgz,mp4,mkv,ogg,flv.
  * `burp_timeoutstop`: value of `TimeoutStopSec` systemd option for Burp service.
                      If `None`, keep systemd default.

### Variables required by server and client:

  `burp`: server configuration (dict)

### Variables required by client:

  `burp_clients`: client configurations (dict)

## Examples

Configuration dicts:
  * Fields marked with `*` are required.
  * Are mentioned below:
    * for required fields: sample values
    * for optional fields: default values if they exist

```yaml
burp:
  dir: '{{ burp_rootdir }}'         # Root directory. 'burp-{{ burp-name }}' unix user must be able to access
                                    # this directory.
  name*: mails
  port*: 4971
  ratelimit:
  ssl*:                             # All files will be copied on the server.
    CA*: '{{ lookup("file", "path/to/ca.pem") }}  # Certificate Authority
    DH*:  '{{ burp_dh }}'           # DH parameters
    cert*: '{{ burp_server_cert }}' # Burp server public certificate
    key*: '{{ burp_server_key }}'   # Burp server private key
burp_clients:
  _server*: backup.corp.example     # inventory hostname of the server
  vmail:                            # unix user, must be able to read paths to
                                    # backup, will be the owner of the certificate
    password*: 'secret'             # used between between client and server
    group:                          # unix group, same as unix user if not set
    ssl*:                           # Both files will be copied on the client.
      cert*: '{{ burp_mail_cert }}' # Burp client public certificate
      key*: '{{ burp_mail_key }}'   # Burp client private key
    backups*:
      main:                         # keys will be used in order to identify different configurations of one client
        paths*:                     # dict of list, keys aren't used
          local: ['/home/vmail/']
        excludes:                   # dict: keys are ignored, values are list of paths to exclude
          path_to_exclude:
            - '/home/vmail/user'

        script_pre:                 # path to a script to run before a backup,
                                    # the arguments to it is 'pre'
        script_pre_args: []         # List of arguments to the pre script

        script_post:                # path to a script to run after a backup,
                                    # the arguments to it is 'post'
        script_post_args: []        # List of arguments to the post script

        script:                     # path to a script to run before and after
                                    # a backup, the arguments to it is 'pre' or
                                    # 'post'. can not be used with 'script_pre'
                                    # or 'script_post'
        script_args: []             # List of arguments to the script run before
                                    # and after a backup.

        cron:
          minute: '0,20,40'         # default value is '*'
          hour: '*'
          day: '*'
          month: '*'
          weekday: '*'
          TZ: 'CEST'                # timezone of the client. If specified, the
                                    # role will fail when the client timezone
                                    # differs.
        timer*:
          args*:                    # one daily backup
            - 24h
            - Mon,Tue,Wed,Thu,Fri,Sat,Sun,22,23,00
          TZ: 'UTC'                 # timezone of the server. If specified, the
                                    # role will fail when the server timezone
                                    # differs.
        keep*: [7, 1, 1]  # 7 days, 1 week, 1 month
        options:                    # a dict, keys can be any option accepted in
                                    # server clientconfir file, for example:
          compression: 'gzip9'
          nobackup: '.nobackup'
          read_all_fifos: 1
```

Another client configuration example:

```yaml
burp_clients:
  _server: backup.corp.example
  anna:
    password: 'secret'
    group: it
    ssl:
      cert: "{{ lookup('file', 'path/to/client_certificate.pem') }}"
      key: "{{ vault_burp_anna_key }}"
    backups:
      source_code:
        paths:
          src: ['/home/anna/src', '/usr/local/src']
          dpkg: ['/var/lib/dpkg/available', '/var/lib/dpkg/diversions']
        excludes:
          data:
              - '/home/dev/iso'
              - '/home/dev/Downloads'
          tmp:
              - '/home/dev/tmp'
              - '/home/dev/.cache'
        cron:
          minute: '0,30'
        # one daily backup
        timer:
          args:
            - 24h
            - Mon,Tue,Wed,Thu,Fri,Sat,Sun,05,06,07
        # 7 days, 1 week, 1 month
        keep: [7, 1, 1]
        options:
          compression: 'gzip0'
```
